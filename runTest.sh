#!/bin/bash -x

make example LDLIBS=-lpthread
seq 1 10 |while read threads; do time ./example ${threads} >& timing-${threads}.txt; done
