#include <iostream>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <ctime>

#include "ThreadPool.h"

int main(int argc, char *argv[])
{
	if (argc < 2) {
		std::cerr << "Need to supply thread count" << std::endl;
	} else {
		srand(time(NULL));

		ThreadPool pool(atoi(argv[1]));
		std::vector< std::string > frames = { "sample_000001.jpg", "sample_000002.jpg", "sample_000003.jpg" };
		std::vector< std::string > models = { "usa", "uk", "iraq", "china", "zimbabwe", "canada", "mexico", "greenland", "france", "germany" };

		for (auto &&frame : frames) {
			std::vector< std::future<int> > results;
			for (auto &&model : models) {
				results.emplace_back(pool.enqueue([frame, model] {
							std::this_thread::sleep_for(std::chrono::milliseconds(rand() % 250));
							std::cout << frame << ": " << model << " done" << std::endl;
							return 0;
							})
						);
			}

			std::cout << "Waiting for " << results.size() << " threads to complete for frame " << frame << std::endl;
			for (auto &&result : results) {
				result.get();
			}
			std::cout << "Finished threads for frame " << frame << std::endl;
		}
	}

	return 0;
}
